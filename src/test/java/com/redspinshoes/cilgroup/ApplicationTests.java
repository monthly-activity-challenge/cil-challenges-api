package com.redspinshoes.cilgroup;

import com.redspinshoes.cilgroup.Entities.Card;
import com.redspinshoes.cilgroup.Entities.ChallengeClass;
import com.redspinshoes.cilgroup.Entities.Participant;
import com.redspinshoes.cilgroup.Repositories.CardRepository;
import com.redspinshoes.cilgroup.Repositories.ChallengeClassRepository;
import com.redspinshoes.cilgroup.Repositories.ParticipantRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ApplicationTests {


    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    ParticipantRepository participantRepository;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    ChallengeClassRepository challengeClassRepository;

    ChallengeService challengeService;
    int classNum = 45;

    @BeforeEach
    void setUp() {
        challengeService = new ChallengeService(participantRepository, cardRepository, challengeClassRepository);

        Participant participant1 = new Participant("Freddy", "Jones", "FredJones", "bike");
        Card participantCard = new Card();
        List<ChallengeClass> classList = challengeService.generateChallengeClasses(participant1.getClassType());
        challengeClassRepository.saveAll(classList);
        participantCard.setClassList(classList);
        cardRepository.save(participantCard);
        participant1.setCard(participantCard);
        participantRepository.save(participant1);

        Participant participant2 = new Participant("Ricky", "Ricardo", "Double_R", "tread");
        Card participantCard2 = new Card();
        List<ChallengeClass> classList2 = challengeService.generateChallengeClasses(participant2.getClassType());
        challengeClassRepository.saveAll(classList2);
        participantCard2.setClassList(classList2);
        cardRepository.save(participantCard2);
        participant2.setCard(participantCard2);
        participantRepository.save(participant2);

    }

    @AfterEach
    void tearDown() {
        participantRepository.deleteAll();
        cardRepository.deleteAll();
        challengeClassRepository.deleteAll();
    }

	@Test
	void contextLoads() {
	}

    @Test
    void howManyEntries() {
        List<BasicParticipantInfo> allParticipantBasic = challengeService.retrieveParticipants();
        assertEquals(2,allParticipantBasic.size());
    }

    @Test
    void retrieveParticipantByLeaderboardName() {
        Optional<Participant> oParticipant = participantRepository.findByLeaderboardName("Double_R");
        assertEquals("Double_R", oParticipant.get().getLeaderboardName());
//        System.out.println(oParticipant.get().getLeaderboardName());
//        System.out.println(oParticipant.get().getId());
//        System.out.println(oParticipant.get().getLastName());
    }

    @Test
    void retrieveCardForLeaderboardName() {

        Card foundCard = challengeService.getCard("FredJones");
        assertEquals(classNum,foundCard.getClassList().size());

        Card foundCard3 = challengeService.getCard("Double_R");
        assertEquals(classNum,foundCard3.getClassList().size());
    }
}
