package com.redspinshoes.cilgroup;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.redspinshoes.cilgroup.Entities.Card;
import com.redspinshoes.cilgroup.Entities.ChallengeClass;
import com.redspinshoes.cilgroup.Entities.Participant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ChallengeController.class)
public class ChallengeControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ChallengeService challengeService;

    ObjectMapper mapper = new ObjectMapper();

    @Test
    void newParticipantAdded() throws Exception {
        //Arrange
        Participant newParticipant = new Participant("Joe", "Smith", "JimmyJ", "bike");

        //Act
        when(challengeService.addParticipant(any(Participant.class))).thenReturn(newParticipant);
        mockMvc.perform(post("/participant")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(newParticipant)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("leaderboardName").value(newParticipant.getLeaderboardName()));
    }

    @Test
    void getParticipants() throws Exception {

        ArrayList<BasicParticipantInfo> participantList = new ArrayList<>();
        participantList.add(new BasicParticipantInfo(5, "Smith"));
        participantList.add(new BasicParticipantInfo(8,"Doe"));
        // Act
        when (challengeService.retrieveParticipants()).thenReturn(participantList);
        mockMvc.perform(get("/participant"))
                .andExpect(status().isOk());
    }

    @Test
    void getParticipantCard() throws Exception {
        String suppliedLeadboardName = "JimmyJ";
        Participant participant = new Participant("Joe", "Smith", "JimmyJ", "tread");
        Card participantCard = new Card();
        ArrayList<ChallengeClass> classList = new ArrayList<>();
        classList.add(new ChallengeClass());
        classList.get(0).setCompletionDate("2021-10-11");
        participantCard.setClassList(classList);

        when(challengeService.getCard(anyString())).thenReturn(participantCard);
        mockMvc.perform(get("/card/" + suppliedLeadboardName))
//                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void updateParticipantCard() throws Exception {
        String suppliedLeadboardName = "JimmyJ";
        Participant participant = new Participant("Joe", "Smith", "JimmyJ", "tread");
        Card participantCard = new Card();

        ArrayList<ChallengeClass> classList = new ArrayList<>();
        classList.add(new ChallengeClass());
        classList.get(0).setInstructorName("Kennedy");
        participantCard.setClassList(classList);


        when(challengeService.updateCard(anyString(), any())).thenReturn(participantCard);
        mockMvc.perform(post("/card/" + suppliedLeadboardName)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(classList)))
                .andExpect(status().isOk());
    }

}
