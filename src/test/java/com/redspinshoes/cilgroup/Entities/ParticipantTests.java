package com.redspinshoes.cilgroup.Entities;

import com.redspinshoes.cilgroup.ChallengeService;
import com.redspinshoes.cilgroup.Repositories.CardRepository;
import com.redspinshoes.cilgroup.Repositories.ChallengeClassRepository;
import com.redspinshoes.cilgroup.Repositories.ParticipantRepository;
import net.minidev.json.JSONUtil;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ParticipantTests {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private CardRepository cardRepository;

   @Autowired
    private ChallengeClassRepository challengeClassRepository;

    private ChallengeService challengeService;

    @Test
    void contextLoads() {
    }

//    @Test
//    void canSaveParticipant() {
//        Participant participant1 = new Participant();
//        assertNull(participant1.getId());
//
//        Card participantCard = new Card();
//        assertNull(participantCard.getId());
//
//        ChallengeClass class1 = new ChallengeClass();
//        assertNull(class1.getClassId());
//
//        class1.setInstructorName("Corbin");
//        class1.setCompletionDate("2021-09-12");
//        class1.setClassType("bike");
//
//        ChallengeClass class2 = new ChallengeClass();
//        assertNull(class2.getClassId());
//
//        class2.setInstructorName("Lovewell");
//        class2.setCompletionDate("2021-10-12");
//        class2.setClassType("tread");
//
//        List<ChallengeClass> classList = new ArrayList<>();
//        classList.add(class1);
//        classList.add(class2);
//
//        challengeClassRepository.saveAll(classList);
//        assertNotNull(class1.getClassId());
//        participantCard.setClassList(classList);
//
//        cardRepository.save(participantCard);
//        assertNotNull(participantCard.getId());
//
//        participant1.setCard(participantCard);
//        participantRepository.save(participant1);
//        assertNotNull(participant1.getId());
//
//        Card savedCard = participant1.getCard();
//        assertEquals("Corbin", savedCard.getClassList().get(0).getInstructorName());
//        System.out.println(participant1.getCard().getClassList().size());
//    }

//    @Test
//    void generateCard() {
//        challengeService = new ChallengeService(participantRepository,cardRepository, challengeClassRepository);
//        Participant newParticipant = new Participant("Joe","Smith","JSmithy","bike");
//        Card participantCard = new Card();
//        List<ChallengeClass> classList = challengeService.generateChallengeClasses(newParticipant.getClassType());
//
//        challengeClassRepository.saveAll(classList);
//
//        participantCard.setClassList(classList);
//        cardRepository.save(participantCard);
//        newParticipant.setCard(participantCard);
//        participantRepository.save(newParticipant);
//
//        assertEquals(30,newParticipant.getCard().getClassList().size());
//    }
}
