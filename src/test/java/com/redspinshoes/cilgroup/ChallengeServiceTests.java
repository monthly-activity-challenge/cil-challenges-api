package com.redspinshoes.cilgroup;

import com.redspinshoes.cilgroup.Entities.Card;
import com.redspinshoes.cilgroup.Entities.ChallengeClass;
import com.redspinshoes.cilgroup.Entities.Participant;
import com.redspinshoes.cilgroup.Repositories.CardRepository;
import com.redspinshoes.cilgroup.Repositories.ChallengeClassRepository;
import com.redspinshoes.cilgroup.Repositories.ParticipantRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ChallengeServiceTests {

    private ChallengeService challengeService;
    @Mock
    ParticipantRepository participantRepository;

    @Mock
    CardRepository cardRepository;

    @Mock
    ChallengeClassRepository challengeClassRepository;

    int classNum = 45;



    @BeforeEach
    void setUp() {
        challengeService = new ChallengeService(participantRepository, cardRepository,challengeClassRepository);
    }

    @Test
    void addNewParticipant(){
        Participant newParticipant = new Participant("Joe", "Smith", "JSmith", "tread");
        Card participantCard = new Card();
        List<ChallengeClass> classList = challengeService.generateChallengeClasses(newParticipant.getClassType());


        when(challengeClassRepository.saveAll(any())).thenReturn(classList);
        when(cardRepository.save(any(Card.class))).thenReturn(participantCard);
        when(participantRepository.save(any(Participant.class))).thenReturn(newParticipant);

        Participant savedParticipant = challengeService.addParticipant(newParticipant);
        assertNotNull(savedParticipant);
        assertEquals(newParticipant.getLeaderboardName(), savedParticipant.getLeaderboardName());
        assertEquals(classNum, savedParticipant.getCard().getClassList().size());
        verify(participantRepository).findByLeaderboardName(anyString());

//        for (int i = 0; i < savedParticipant.getCard().getClassList().size(); i++) {
//            System.out.println(i + "  " + savedParticipant.getCard().getClassList().get(i).getInstructorName() + "  " +
//                    savedParticipant.getCard().getClassList().get(i).getClassType());
//        }
    }

    @Test
    void retrieveAllParticipants() {
        List<Participant> participantList = new ArrayList<>();
        participantList.add(new Participant("Joe", "Smith", "JSmith", "tread"));
        participantList.add(new Participant("Jill", "Jones", "Jonsie", "bike"));

        when(participantRepository.findAll()).thenReturn(participantList);
        List<BasicParticipantInfo> returnedParticipants = challengeService.retrieveParticipants();

        assertNotNull(returnedParticipants);
        assertFalse(returnedParticipants.isEmpty());
        assertEquals(2,returnedParticipants.size());
    }

    @Test
    void retrieveParticipantCard() {
        Participant participant = new Participant("Joe", "Smith", "JSmith", "tread");
        Card participantCard = new Card();
        List<ChallengeClass> classList = challengeService.generateChallengeClasses(participant.getClassType());

        when(challengeClassRepository.saveAll(any())).thenReturn(classList);
        when(cardRepository.save(any(Card.class))).thenReturn(participantCard);
        when(participantRepository.save(any(Participant.class))).thenReturn(participant);
        challengeClassRepository.saveAll(classList);
        participantCard.setClassList(classList);
        cardRepository.save(participantCard);
        participant.setCard(participantCard);
        participantRepository.save(participant);

        when(participantRepository.findByLeaderboardName(anyString())).thenReturn(java.util.Optional.of(participant));
        Card foundParticipantCard = challengeService.getCard(participant.getLeaderboardName());

        assertNotNull(foundParticipantCard);
        assertEquals(classNum, foundParticipantCard.getClassList().size());
    }

    @Test
    void saveCardUpdates() {
        Participant participant = new Participant("Joe", "Smith", "JSmith", "tread");
        Card participantCard = new Card();
        List<ChallengeClass> classList = challengeService.generateChallengeClasses(participant.getClassType());

        when(challengeClassRepository.saveAll(any())).thenReturn(classList);
        when(cardRepository.save(any(Card.class))).thenReturn(participantCard);
        when(participantRepository.save(any(Participant.class))).thenReturn(participant);
        challengeClassRepository.saveAll(classList);
        participantCard.setClassList(classList);
        cardRepository.save(participantCard);
        participant.setCard(participantCard);
        participantRepository.save(participant);

        participant.getCard().getClassList().get(10).setCompletionDate("2021-11-06");
        participant.getCard().getClassList().get(20).setCompletionDate("2021-11-04");
        when(participantRepository.findByLeaderboardName(participant.getLeaderboardName())).thenReturn(java.util.Optional.of(participant));

        List<ChallengeClass> updatedClasses = new ArrayList<>();
        updatedClasses.add(participant.getCard().getClassList().get(10));
        updatedClasses.add(participant.getCard().getClassList().get(11));

        Card updatedCard = challengeService.updateCard(participant.getLeaderboardName(), updatedClasses );

        assertNotNull(updatedCard);
        assertEquals(classNum, updatedCard.getClassList().size());
    }
}
