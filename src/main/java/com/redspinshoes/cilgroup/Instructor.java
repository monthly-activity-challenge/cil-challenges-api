package com.redspinshoes.cilgroup;

public class Instructor {
    private String instructorName;
    private String classType;
    private int useCount;

    public Instructor(String instructorName, String classType) {
        this.instructorName = instructorName;
        this.classType = classType;
        this.useCount = 0;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

    public void incrementUseCount() {
        this.useCount++;
    }
}
