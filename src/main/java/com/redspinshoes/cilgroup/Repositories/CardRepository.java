package com.redspinshoes.cilgroup.Repositories;

import com.redspinshoes.cilgroup.Entities.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {
}
