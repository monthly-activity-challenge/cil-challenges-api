package com.redspinshoes.cilgroup.Repositories;

import com.redspinshoes.cilgroup.Entities.ChallengeClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChallengeClassRepository extends JpaRepository<ChallengeClass, Integer> {
}
