package com.redspinshoes.cilgroup;

import java.util.Random;

public class InstructorClasses {
    private Instructor[] instructorClasses = new Instructor[135];

    public InstructorClasses() {
        this.instructorClasses[0] = new Instructor("Alex Toussaint", "bike");
        this.instructorClasses[1] = new Instructor("Ally Love", "bike");
        this.instructorClasses[2] = new Instructor("Ben Alldis", "bike");
        this.instructorClasses[3] = new Instructor("Bradley Rose", "bike");
        this.instructorClasses[4] = new Instructor("Camila Ramon", "bike");
        this.instructorClasses[5] = new Instructor("German Instructor", "bike");
        this.instructorClasses[6] = new Instructor("Christine D'Ercole", "bike");
        this.instructorClasses[7] = new Instructor("Cody Rigsby", "bike");
        this.instructorClasses[8] = new Instructor("Denis Morton", "bike");
        this.instructorClasses[9] = new Instructor("Emma Lovewell", "bike");
        this.instructorClasses[10] = new Instructor("Hannah Corbin", "bike");
        this.instructorClasses[11] = new Instructor("Hannah Frankson", "bike");
        this.instructorClasses[12] = new Instructor("Jenn Sherman", "bike");
        this.instructorClasses[13] = new Instructor("Jess King", "bike");
        this.instructorClasses[14] = new Instructor("Kendall Toole", "bike");
        this.instructorClasses[15] = new Instructor("Leanne Hainsby", "bike");
        this.instructorClasses[16] = new Instructor("Matt Wilpers", "bike");
        this.instructorClasses[17] = new Instructor("Olivia Amato", "bike");
        this.instructorClasses[18] = new Instructor("Robin Arzon", "bike");
        this.instructorClasses[19] = new Instructor("Sam Yo", "bike");
        this.instructorClasses[20] = new Instructor("Tunde Oyeneyin", "bike");
        this.instructorClasses[21] = new Instructor("Ben Alldis", "power zone");
        this.instructorClasses[22] = new Instructor("Christine D'Ercole", "power zone");
        this.instructorClasses[23] = new Instructor("Denis Morton", "power zone");
        this.instructorClasses[24] = new Instructor("Matt Wilpers", "power zone");
        this.instructorClasses[25] = new Instructor("Olivia Amato", "power zone");
        this.instructorClasses[26] = new Instructor("Adrian Williams", "tread");
        this.instructorClasses[27] = new Instructor("Andy Speer", "tread");
        this.instructorClasses[28] = new Instructor("Becs Gentry", "tread");
        this.instructorClasses[29] = new Instructor("Chase Tucker", "tread");
        this.instructorClasses[30] = new Instructor("Daniel McKenna", "tread");
        this.instructorClasses[31] = new Instructor("Jermaine Johnson", "tread");
        this.instructorClasses[32] = new Instructor("Jess Sims", "tread");
        this.instructorClasses[33] = new Instructor("Jess King", "tread");
        this.instructorClasses[34] = new Instructor("Jon Hosking", "tread");
        this.instructorClasses[35] = new Instructor("Joslyn Thompson Rule", "tread");
        this.instructorClasses[36] = new Instructor("Kirsten Ferguson", "tread");
        this.instructorClasses[37] = new Instructor("Marcel Dinkins", "tread");
        this.instructorClasses[38] = new Instructor("German Instructor", "tread");
        this.instructorClasses[39] = new Instructor("Matt Wilpers", "tread");
        this.instructorClasses[40] = new Instructor("Matty Maggiacomo", "tread");
        this.instructorClasses[41] = new Instructor("Olivia Amato", "tread");
        this.instructorClasses[42] = new Instructor("Rebecca Kennedy", "tread");
        this.instructorClasses[43] = new Instructor("Robin Arzon", "tread");
        this.instructorClasses[44] = new Instructor("Selena Samuela", "tread");
        this.instructorClasses[45] = new Instructor("Susie Chan", "tread");
        this.instructorClasses[46] = new Instructor("Aditi Shah", "yoga");
        this.instructorClasses[47] = new Instructor("Anna Greenberg", "yoga");
        this.instructorClasses[48] = new Instructor("Chelsea Jackson Roberts", "yoga");
        this.instructorClasses[49] = new Instructor("Denis Morton", "yoga");
        this.instructorClasses[50] = new Instructor("Kirra Michel", "yoga");
        this.instructorClasses[51] = new Instructor("Kristin McGee", "yoga");
        this.instructorClasses[52] = new Instructor("Mariana Fernandez", "yoga");
        this.instructorClasses[53] = new Instructor("Ross Rayburn", "yoga");
        this.instructorClasses[54] = new Instructor("Aditi Shah", "meditation");
        this.instructorClasses[55] = new Instructor("Anna Greenberg", "meditation");
        this.instructorClasses[56] = new Instructor("Chelsea Jackson Roberts", "meditation");
        this.instructorClasses[57] = new Instructor("Cody Rigsby", "meditation");
        this.instructorClasses[58] = new Instructor("Denis Morton", "meditation");
        this.instructorClasses[59] = new Instructor("Kirra Michel", "meditation");
        this.instructorClasses[60] = new Instructor("Kristin McGee", "meditation");
        this.instructorClasses[61] = new Instructor("Ross Rayburn", "meditation");
        this.instructorClasses[62] = new Instructor("Callie Gullickson", "strength");
        this.instructorClasses[63] = new Instructor("Rad Lopez", "strength");
        this.instructorClasses[64] = new Instructor("Aditi Shah", "strength");
        this.instructorClasses[65] = new Instructor("Adrian Williams", "strength");
        this.instructorClasses[66] = new Instructor("Alex Toussaint", "strength");
        this.instructorClasses[67] = new Instructor("Alley Love", "strength");
        this.instructorClasses[68] = new Instructor("Andy Speer", "strength");
        this.instructorClasses[69] = new Instructor("Anna Greenberg", "strength");
        this.instructorClasses[70] = new Instructor("Becs Gentry", "strength");
        this.instructorClasses[71] = new Instructor("Ben Alldis", "strength");
        this.instructorClasses[72] = new Instructor("Bradley Rose", "strength");
        this.instructorClasses[73] = new Instructor("Chase Tucker", "strength");
        this.instructorClasses[74] = new Instructor("Christine D'Ercole", "strength");
        this.instructorClasses[75] = new Instructor("Cody Rigsby", "strength");
        this.instructorClasses[76] = new Instructor("Daniel McKenna", "strength");
        this.instructorClasses[77] = new Instructor("Denis Morton", "strength");
        this.instructorClasses[78] = new Instructor("Emma Lovewell", "strength");
        this.instructorClasses[79] = new Instructor("Hannah Corbin", "strength");
        this.instructorClasses[80] = new Instructor("Hannah Frankson", "strength");
        this.instructorClasses[81] = new Instructor("Jenn Sherman", "strength");
        this.instructorClasses[82] = new Instructor("Jess Sims", "strength");
        this.instructorClasses[83] = new Instructor("Jess King", "strength");
        this.instructorClasses[84] = new Instructor("Kendall Toole", "strength");
        this.instructorClasses[85] = new Instructor("Kristen McGee", "strength");
        this.instructorClasses[86] = new Instructor("Leanne Hainsby", "strength");
        this.instructorClasses[87] = new Instructor("Matt Wilpers", "strength");
        this.instructorClasses[88] = new Instructor("Matty Maggiacomo", "strength");
        this.instructorClasses[89] = new Instructor("Olivia Amato", "strength");
        this.instructorClasses[90] = new Instructor("Rebecca Kennedy", "strength");
        this.instructorClasses[91] = new Instructor("Robin Arzon", "strength");
        this.instructorClasses[92] = new Instructor("Sam Yo", "strength");
        this.instructorClasses[93] = new Instructor("Selena Samuela", "strength");
        this.instructorClasses[94] = new Instructor("Tunde Oyeneyin", "strength");
        this.instructorClasses[95] = new Instructor("HIIT Cardio", "other");
        this.instructorClasses[96] = new Instructor("Dance Cardio", "other");
        this.instructorClasses[97] = new Instructor("Turkey Trot - outdoor", "other");
        this.instructorClasses[98] = new Instructor("Robin Arzon - outdoor", "other");
        this.instructorClasses[99] = new Instructor("Kirsten Ferguson - outdoor", "other");
        this.instructorClasses[100] = new Instructor("Bootcamp", "other");
        this.instructorClasses[101] = new Instructor("Adrian Williams", "stretch");
        this.instructorClasses[102] = new Instructor("Alex Toussaint", "stretch");
        this.instructorClasses[103] = new Instructor("Ally Love", "stretch");
        this.instructorClasses[104] = new Instructor("Andy Speer", "stretch");
        this.instructorClasses[105] = new Instructor("Becs Gentry", "stretch");
        this.instructorClasses[106] = new Instructor("Ben Alldis", "stretch");
        this.instructorClasses[107] = new Instructor("Chase Tucker", "stretch");
        this.instructorClasses[108] = new Instructor("Christine D'Ercole", "stretch");
        this.instructorClasses[109] = new Instructor("Cody Rigsby", "stretch");
        this.instructorClasses[110] = new Instructor("Daniel McKenna", "stretch");
        this.instructorClasses[111] = new Instructor("Denis Morton", "stretch");
        this.instructorClasses[112] = new Instructor("Emma Lovewell", "stretch");
        this.instructorClasses[113] = new Instructor("Hannah Corbin", "stretch");
        this.instructorClasses[114] = new Instructor("Hannah Frankson", "stretch");
        this.instructorClasses[115] = new Instructor("Jenn Sherman", "stretch");
        this.instructorClasses[116] = new Instructor("Jess Sims", "stretch");
        this.instructorClasses[117] = new Instructor("Jess King", "stretch");
        this.instructorClasses[118] = new Instructor("Kendall Toole", "stretch");
        this.instructorClasses[119] = new Instructor("Leanne Hainsby", "stretch");
        this.instructorClasses[120] = new Instructor("Matt Wilpers", "stretch");
        this.instructorClasses[121] = new Instructor("Matty Maggiacomo", "stretch");
        this.instructorClasses[122] = new Instructor("Olivia Amato", "stretch");
        this.instructorClasses[123] = new Instructor("Rebecca Kennedy", "stretch");
        this.instructorClasses[124] = new Instructor("Robin Arzon", "stretch");
        this.instructorClasses[125] = new Instructor("Sam Yo", "strength");
        this.instructorClasses[126] = new Instructor("Selena Samuela", "stretch");
        this.instructorClasses[127] = new Instructor("Tunde Oyeneyin", "stretch");
        this.instructorClasses[128] = new Instructor("Susie Chan", "stretch");
        this.instructorClasses[129] = new Instructor("Marcel Dinkins", "stretch");
        this.instructorClasses[130] = new Instructor("Kirsten Ferguson", "stretch");
        this.instructorClasses[131] = new Instructor("Just Ride/Run", "other");
        this.instructorClasses[132] = new Instructor("Scenic Ride/Run", "other");
        this.instructorClasses[133] = new Instructor("Activity Challenge-Gold", "other");
        this.instructorClasses[134] = new Instructor("Ride/Run Challenge-Silver", "other");




    }

    public Instructor[] getInstructorClasses() {
        return instructorClasses;
    }

    public void setInstructorClasses(Instructor[] instructorClasses) {
        this.instructorClasses = instructorClasses;
    }

    public Instructor getInstructorPrimaryType(String primaryType, int useNum) {

        boolean instructorClassFound = false;
        Random rand = new Random();
        int rand_int = 0;

        while (!instructorClassFound) {
            rand_int = rand.nextInt(this.instructorClasses.length);

            if (this.instructorClasses[rand_int].getClassType().equals(primaryType) && (this.instructorClasses[rand_int].getUseCount() < useNum)) {
                this.instructorClasses[rand_int].incrementUseCount();
                instructorClassFound = true;
            }
        }
        return this.instructorClasses[rand_int];
    }

    public Instructor getInstructorNonprimaryType(String primaryType, int useNum) {

        boolean instructorClassFound = false;
        Random rand = new Random();
        int rand_int = 0;

        while (!instructorClassFound) {
            rand_int = rand.nextInt(this.instructorClasses.length);

            if (this.instructorClasses[rand_int].getClassType() != primaryType && this.instructorClasses[rand_int].getUseCount() < useNum) {
                this.instructorClasses[rand_int].incrementUseCount();
                instructorClassFound = true;
            }
        }
        return this.instructorClasses[rand_int];
    }
}
