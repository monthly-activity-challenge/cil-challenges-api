package com.redspinshoes.cilgroup;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.redspinshoes.cilgroup.Entities.Participant;

import java.util.ArrayList;
import java.util.List;

public class BasicParticipantInfo {
    private Integer id;
    private String leaderboardName;

    public BasicParticipantInfo(Integer id, String leaderboardName) {
        this.id = id;
        this.leaderboardName = leaderboardName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLeaderboardName() {
        return leaderboardName;
    }

    public void setLeaderboardName(String leaderboardName) {
        this.leaderboardName = leaderboardName;
    }
}
