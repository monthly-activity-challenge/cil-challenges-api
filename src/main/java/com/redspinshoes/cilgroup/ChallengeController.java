package com.redspinshoes.cilgroup;

import com.redspinshoes.cilgroup.Entities.Card;
import com.redspinshoes.cilgroup.Entities.ChallengeClass;
import com.redspinshoes.cilgroup.Entities.Participant;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
public class ChallengeController {

    private ChallengeService challengeService;

    public ChallengeController(ChallengeService challengeService) {
        this.challengeService = challengeService;
    }

    @PostMapping("/participant")
    public ResponseEntity<Participant> newParticipant(@RequestBody Participant participant){
        return ResponseEntity.ok(challengeService.addParticipant(participant));
    }

    @GetMapping("/participant")
    public ResponseEntity<ArrayList> retrieveParticipant() {

        return ResponseEntity.ok(challengeService.retrieveParticipants());
    }

    @GetMapping("/card/{leaderboardName}")
    public ResponseEntity<Card> getCard(@PathVariable String leaderboardName) {
        Card participantCard = challengeService.getCard(leaderboardName);
        return (participantCard == null) ? ResponseEntity.noContent().build() : ResponseEntity.ok(participantCard);
    }

    @PostMapping("/card/{leaderboardName}")
    public ResponseEntity<Card> updateCard(@PathVariable String leaderboardName,
                                                 @RequestBody ArrayList<ChallengeClass> updatedClasses) {
        Card participantCard = challengeService.updateCard(leaderboardName, updatedClasses);
        return (participantCard == null) ? ResponseEntity.noContent().build() : ResponseEntity.ok(participantCard);
    }

}
